# img2simg container

Ths is a simple repo to build a Fedora container with the
`andoid-tools` installed, so you can use `img2simg` without
installing in your host.

## Build the container

```shell
podman build -t image2simg .
```

## Use it

```shell
podman run -it --rm -v ${PWD}:/images:z -w /images img2simg:latest /bin/img2simg image.ext4 image.ext4.simg
```

